exports.config = {
		
		 directConnect: true,
	  multiCapabilities: [{
				browserName: 'chrome'
			}],
			
		/*	capabilities: {
			    'browserName': 'chrome',
			    'platform': 'ANY',
			    'version': 'ANY',
			    'chromeOptions': {
			        // Get rid of --ignore-certificate yellow warning
			        args: ['--no-sandbox', '--test-type=browser'],
			        // Set download path and avoid prompting for download even though
			        // this is already the default on Chrome but for completeness
			        prefs: {
			            'download': {
			                'prompt_for_download': false,
			                'directory_upgrade': true,
			                'default_directory': 'Downloads',
			            }
			        }
			    }
			}, */
		
		 framework: 'jasmine2',
			  seleniumAddress: 'http://localhost:4444/wd/hub',
	/*	specs: [
			     './Test_Specs/File_Upload.js',
			       './Test_Specs/Drag_and_Drop.js',
			     './Test_Specs/Mouse_hover.js'
			     './Test_Specs/DataDriven.js',
			     ],
			  
			   
			  
	    */
	
		specs:     './Test_Specs/Mouse_hover.js',
			
			  jasmineNodeOpts: {
				  
			  defaultTimeoutInterval:900000
		 },
			      
		onPrepare: function() {
			browser.waitForAngularEnabled(false); 
			    	    var AllureReporter = require('jasmine-allure-reporter');
			    	    jasmine.getEnv().addReporter(new AllureReporter({
			    	      resultsDir: 'allure-results'
			    	    }));
			    	
                   } 
			      
		
			    	
		};